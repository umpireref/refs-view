/* Getters.js
  - Computed properties from state
*/

/* example
export const numOfFixtures = state => {
  return state.fixtures.length
}
*/
import * as types from './utils/mutation-types'
