/* Mutations */
export const GET_FIXTURES = 'GET_FIXTURES'
export const GET_GAME = 'GET_GAME'

/* Actions */
// All Fixtures
export const getFixtures = 'getFixtures'
export const getFixturesSuccess = 'getFixturesSuccess'
export const getFixturesFailure = 'getFixturesFailure'

// Single Game
export const getGame = 'getGame'
export const getGameSuccess = 'getGameSuccess'
export const getGameFailure = 'getGameFailure'
