/* Actions.js
  - Actions call mutations to change state through commits
  - Actions handle asynchronous requests
*/
import api from './utils/api'
import * as types from './utils/mutation-types'

const BASE_URL = 'https://umpireref.herokuapp.com/api/v1/'

const actions = {
  [types.getFixtures] ({commit}) {
    api.get(BASE_URL + 'fixtures/')
      .then((response) => commit(
        types.GET_FIXTURES,
        response.body.fixtures))
      .catch((error) => console.log('API ERROR'));
    // .catch((error) => commit('API_FAILURE', error));
  },

  [types.getGame] ({commit}, id) {
    api.get(BASE_URL + 'fixtures/'+ id)
      .then((response) => commit(types.GET_GAME, response.body))
      .catch((error) => console.log('API ERROR'));
  }
}

export default actions
