/* Mutations.js
  - Application level state can only be modified through mutations
  - Mutations are synchronous
*/
import * as types from './utils/mutation-types'

const mutations = {
  [types.GET_FIXTURES] (state, games) {
    state.fixtures = games
  },

  [types.GET_GAME] (state, game) {
    state.game = game
  }
}

export default mutations
