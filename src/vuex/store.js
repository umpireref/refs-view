import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'

Vue.use(Vuex)

// disable strict mode in production
const debug = process.env.NODE_ENV !== 'production'

// initial application state
const state = {
  fixtures: [],
  game: null
}

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  strict: debug
})
