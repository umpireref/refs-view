import Vue from 'vue'
import VueRouter from 'vue-router'
import Resource from 'vue-resource'
import Materials from 'vue-materials'

// Components
import App from './components/App.vue'
import AllGames from './components/AllGames.vue'
import SingleGame from './components/SingleGame.vue'
import GameMenu from './components/GameMenu.vue'
import Prematch from './components/Prematch.vue'
import Login from './components/Login.vue'
import store from './vuex/store'

// Installs
Vue.use(VueRouter)
Vue.use(Resource)
Vue.use(Materials)

// Routing
const routes = [
    {
      path: '/',
      name: 'index',
      component: AllGames
    },
    {
      path: '/game/:game_id',
      name: 'get_game',
      component: SingleGame
    },
    {
      path: '/game/prematch/:game_id',
      name: 'prematch',
      component: Prematch
    },
    {
      path: '/game/:game_id/game_menu',
      name: 'game_menu',
      component: GameMenu
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '*',
      redirect: { name: 'index' }
    }
  ]

const router = new VueRouter({
  routes
})

router.afterEach((to, from) => {

  // change body background colours on route
  switch(to.name){
    case "game_menu":
      document.body.className = 'bg-white'
      break;
    default:
      document.body.className = 'bg-primary'
  }
})

const app = new Vue({
  el: "#app",
  router,
  store,
  render: h => h(App)
})
