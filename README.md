# Umpireref App

> Football referee app for managing fixtures and officiating games

# Version: Pre-Alpha

## Architecure

This web app is built using VueJS, Vue-loader, Vue-resource and Vuex architecture. The frontend communcates to a Rails backend.

---

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

---

## Contributing

If you'd like to contribute to this project, that's awesome, and we <3 you. There's a guide to contributing to umpireref (both code and general help) over in [Contributing](https://gitlab.com/umpireref/refs-view/blob/master/Contributing.md)
